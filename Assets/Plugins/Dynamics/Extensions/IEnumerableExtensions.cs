﻿using System.Collections.Generic;

namespace Dynamics {
    public static class IEnumerableExtensions {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source) {
            return new HashSet<T>(source);
        }
    }
}
