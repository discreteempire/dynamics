﻿using System;

namespace Dynamics.CommandHandling {
    public class CommandMessage : ICommandMessage {
        public Type type {
            get;
            private set;
        }

        public object payload {
            get;
            private set;
        }

        public static ICommandMessage Create<T>(T payload) {
            return new CommandMessage(typeof(T), payload);
        }

        private CommandMessage(Type type, object payload) {
            this.type = payload.GetType();
            this.payload = payload;
        }
    }
}
