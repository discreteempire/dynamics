﻿using System;

namespace Dynamics.CommandHandling {
    public interface ICommandHandler : IObserver<object> {
        Type type { get; }
    }

    public interface ICommandHandler<T> : ICommandHandler {
    }
}
