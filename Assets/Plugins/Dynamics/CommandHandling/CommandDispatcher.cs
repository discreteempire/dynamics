﻿using Dynamics.Reactive;
using System;
using System.Collections.Generic;

namespace Dynamics.CommandHandling {
    public class CommandDispatcher : ICommandDispatcher {
        private IScheduler scheduler;
        private IDictionary<Type, ICommandHandler> handlers = new Dictionary<Type, ICommandHandler>();

        public CommandDispatcher(IScheduler scheduler) {
            this.scheduler = scheduler;
        }

        public void OnCompleted() {
            foreach (var handler in handlers.Values) {
                scheduler.Schedule(() => {
                    handler.OnCompleted();
                });
            }
        }

        public void OnError(Exception error) {
            throw new NotImplementedException();
        }

        public void OnNext(ICommandMessage value) {
            ICommandHandler handler;

            if (handlers.TryGetValue(value.type, out handler)) {
                scheduler.Schedule(() => {
                    try {
                        handler.OnNext(value.payload);
                    } catch (Exception e) {
                        handler.OnError(e);
                    }
                });
            } else {
                throw new CommandHandlerNotFoundException();
            }
        }

        public IDisposable Subscribe(ICommandHandler handler) {
            return Add(handler);
        }

        private Subscription Add(ICommandHandler handler) {
            if (handlers.ContainsKey(handler.type)) {
                throw new CommandHandlerAlreadySubscribedException();
            }

            handlers.Add(handler.type, handler);

            return new Subscription(this, handler);
        }

        private void Remove(ICommandHandler handler) {
            handlers.Remove(handler.type);

            scheduler.Schedule(() => {
                handler.OnCompleted();    
            });
        }

        class Subscription : IDisposable {
            private CommandDispatcher dispatcher;
            private ICommandHandler handler;

            public Subscription(CommandDispatcher dispatcher, ICommandHandler handler) {
                this.dispatcher = dispatcher;
                this.handler = handler;
            }

            public void Dispose() {
                dispatcher.Remove(handler);
            }
        }
    }
}
