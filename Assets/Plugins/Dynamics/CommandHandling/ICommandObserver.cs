﻿using System;

namespace Dynamics.CommandHandling {
    public interface ICommandObserver : IObserver<ICommandMessage> {
    }
}
