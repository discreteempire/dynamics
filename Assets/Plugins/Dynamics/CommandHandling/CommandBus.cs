﻿using System;
using Dynamics.Reactive;
using System.Collections.Generic;

namespace Dynamics.CommandHandling {
    public class CommandBus : ICommandBus {
        private IScheduler scheduler;
        private IList<ICommandObserver> observers = new List<ICommandObserver>();

        public CommandBus(IScheduler scheduler) {
            this.scheduler = scheduler;
        }

        public void Dispatch(ICommandMessage command) {
            foreach (var observer in observers) {
                scheduler.Schedule(() => {
                    try {
                        observer.OnNext(command);
                    } catch (Exception e) {
                        observer.OnError(e);
                    }
                });
            }
        }

        public IDisposable Subscribe(ICommandObserver observer) {
            return Add(observer);
        }

        private Subscription Add(ICommandObserver observer) {
            observers.Add(observer);

            return new Subscription(this, observer);
        }

        private void Remove(ICommandObserver observer) {
            observers.Remove(observer);

            scheduler.Schedule(() => {
                observer.OnCompleted();
            });
        }

        public void Dispose() {
            foreach (var observer in observers) {
                scheduler.Schedule(() => {
                    observer.OnCompleted();
                });
            }
        }

        class Subscription : IDisposable {
            private CommandBus commandBus;
            private ICommandObserver observer;

            public Subscription(CommandBus commandBus, ICommandObserver observer) {
                this.commandBus = commandBus;
                this.observer = observer;
            }

            public void Dispose() {
                commandBus.Remove(observer);
            }
        }
    }
}
