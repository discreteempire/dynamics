﻿using System;

namespace Dynamics.CommandHandling {
    public interface ICommandDispatcher : ICommandObserver {
        IDisposable Subscribe(ICommandHandler handler);
    }
}
