﻿using System;

namespace Dynamics.CommandHandling {
    public interface ICommandBus : IDisposable {
        void Dispatch(ICommandMessage command);

        IDisposable Subscribe(ICommandObserver observer);
    }
}
