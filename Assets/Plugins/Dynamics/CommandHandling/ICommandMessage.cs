﻿using System;

namespace Dynamics.CommandHandling {
    public interface ICommandMessage {
        Type type {
            get;
        }

        object payload {
            get;
        }
    }
}
