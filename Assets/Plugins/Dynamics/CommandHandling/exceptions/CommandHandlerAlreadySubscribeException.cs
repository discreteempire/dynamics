﻿using System;

namespace Dynamics.CommandHandling {
    public class CommandHandlerAlreadySubscribedException : Exception {
        internal CommandHandlerAlreadySubscribedException() {
        }
    }
}