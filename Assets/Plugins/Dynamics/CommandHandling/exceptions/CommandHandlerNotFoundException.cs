﻿using System;

namespace Dynamics.CommandHandling {
    public class CommandHandlerNotFoundException : Exception {
        internal CommandHandlerNotFoundException() {
        }
    }
}