﻿using System;

namespace Dynamics.CommandHandling {
    public static class CommandHandler {
        public static ICommandHandler Create<T>(Action<T> action) {
            return new CommandHandler<T>(typeof(T), action);
        }
    }

    public class CommandHandler<T> : ICommandHandler<T> {
        private Action<T> action;

        public Type type {
            get;
            private set;
        }

        internal CommandHandler(Type type, Action<T> action) {
            this.type = type;
            this.action = action;
        }

        public void OnNext(object value) {
            action((T)value);
        }

        public void OnError(Exception error) {
            throw new NotImplementedException();
        }

        public void OnCompleted() {
            throw new NotImplementedException();
        }
    }
}
