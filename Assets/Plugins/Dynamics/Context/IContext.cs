﻿using System;

namespace Dynamics.Context {
    public interface IContext {
        IDisposable AddConfigution(IConfiguration configuration);
        T Resolve<T>();
    }
}
