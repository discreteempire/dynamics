﻿using System;

namespace Dynamics.Context {
    public static class Context {
        private static LocalContext context = new LocalContext();

        public static IDisposable AddConfigution(IConfiguration configuration) {
            return context.AddConfigution(configuration);
        }

        public static T Resolve<T>() {
            return context.Resolve<T>();
        }
    }
}
