﻿using System;

namespace Dynamics.Context {
    public class LocalContext : IContext {
        private IContainer container = new Container();

        public IDisposable AddConfigution(IConfiguration configuration) {
            container.AddBindings(configuration.bindings);
            return new Registaration(this, configuration);
        }

        public T Resolve<T>() {
            return (T)container.Resolve(typeof(T));
        }

        private void Remove(IConfiguration configuration) {
            container.RemoveBindings(configuration.bindings);
        }

        class Registaration : IDisposable {
            private readonly LocalContext context;
            private readonly IConfiguration configuration;

            public Registaration(LocalContext context, IConfiguration configuration) {
                this.context = context;
                this.configuration = configuration;
            }

            public void Dispose() {
                context.Remove(configuration);
            }
        }
    }
}
