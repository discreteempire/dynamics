﻿using System;
using System.Collections.Generic;

namespace Dynamics.Context {
    public interface IContainer {
        void AddBindings(IEnumerable<IBinding> bindings);
        void RemoveBindings(IEnumerable<IBinding> bindings);

        object Resolve(Type type);
        List<object> Resolve(List<Type> type);
    }
}
