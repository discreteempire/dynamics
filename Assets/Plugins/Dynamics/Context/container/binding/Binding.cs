﻿using System;

namespace Dynamics.Context {
    internal class Binding : IBinding {
        public Type service {
            get;
            private set;
        }

        public Type implementation {
            get;
            private set;
        }

        public IProvider provider {
            get;
            private set;
        }

        public Binding(Type service, Type implementation, IProvider provider) {
            this.service = service;
            this.implementation = implementation;
            this.provider = provider;
        }
    }
}
