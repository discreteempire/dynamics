﻿using System;

namespace Dynamics.Context {
    public interface IBinding {
        Type service { get; }
        Type implementation { get; }
        IProvider provider { get; }
    }
}