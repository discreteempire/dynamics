﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Dynamics.Context {
    public class Container : IContainer {
        private Dictionary<Type, IBinding> bindings;
        private Dictionary<Type, object> instances;

        public Container() {
            bindings = new Dictionary<Type, IBinding>();
            instances = new Dictionary<Type, object>();
        }

        public void AddBindings(IEnumerable<IBinding> bindings) {
            CheckCycle(bindings);
            CheckNeededServices(bindings);

            foreach (IBinding binding in bindings) {
                this.bindings.Add(binding.service, binding);
            }
        }

        private void CheckCycle(IEnumerable<IBinding> bindings) {
            HashSet<Type> visited = new HashSet<Type>();
            Dictionary<Type, IBinding> bindingsMap = bindings.ToDictionary(t => t.service, t => t);

            Stack<Type> stack = new Stack<Type>(bindings.SelectMany(b => b.provider.parameters));

            while (stack.Count != 0) {
                Type type = stack.Pop();

                if (visited.Contains(type)) {
                    throw new CycleFoundInBindings();
                } else {
                    visited.Add(type);

                    IBinding binding;
                    if (bindingsMap.TryGetValue(type, out binding)) {
                        binding.provider.parameters.ForEach(t => stack.Push(t));
                    }
                }
            }
        }

        private void CheckNeededServices(IEnumerable<IBinding> bindings) {
            HashSet<Type> bindingTypes = bindings.Select(b => b.service).ToHashSet();

            foreach (IBinding binding in bindings) {
                foreach (Type parameter in binding.provider.parameters) {
                    if (!bindingTypes.Contains(parameter) && !this.bindings.ContainsKey(parameter)) {
                        throw new MissingServiceInjectParameter(binding.service, parameter);
                    }
                }
            }
        }

        public void RemoveBindings(IEnumerable<IBinding> bindings) {
            foreach (IBinding binding in bindings) {
                this.bindings.Remove(binding.service);
                this.instances.Remove(binding.service);
            }
        }

        public object Resolve(Type type) {
            object instance;

            if (!instances.TryGetValue(type, out instance)) {
                IBinding binding;

                if (bindings.TryGetValue(type, out binding)) {
                    instance = binding.provider.Create(this);
                    instances.Add(type, instance);
                } else {
                    throw new BindNotFoundException(type);
                }
            }

            return instance;
        }

        public List<object> Resolve(List<Type> types) {
            return types.Select(t => Resolve(t)).ToList();
        }
    }
}
