﻿using System;

namespace Dynamics.Context {
    public class ConstructorBindingBuilder {
        private Type service;
        private Type implementation;

        internal static ConstructorBindingBuilder Create(Type service, Type implementation) {
            if (!service.IsAssignableFrom(implementation)) {
                throw new ServiceIsNotAssignableFromImplementation(service, implementation);
            }

            return new ConstructorBindingBuilder(service, implementation);
        }

        private ConstructorBindingBuilder(Type service, Type implementation) {
            this.service = service;
            this.implementation = implementation;
        }

        public IBinding Build() {
            return new Binding(service, implementation, ConstructorProvider.Create(implementation));
        }
    }
}
