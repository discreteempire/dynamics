﻿using System;

namespace Dynamics.Context {
    public class BindingBuilder {
        private Type service;

        private BindingBuilder(Type service) {
            this.service = service;
        }

        public static BindingBuilder Bind<TService>() {
            return new BindingBuilder(typeof(TService));
        }

        public ConstantBindingBuilder ToConstant(object value) {
            return ConstantBindingBuilder.Create(service, value);
        }

        public ConstructorBindingBuilder To<TImplementation>() {
            return ConstructorBindingBuilder.Create(service, typeof(TImplementation));
        }

        public IBinding Build() {
            return new Binding(service, service, ConstructorProvider.Create(service));
        }
    }
}
