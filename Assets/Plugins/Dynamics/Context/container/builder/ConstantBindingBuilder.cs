﻿using System;

namespace Dynamics.Context {
    public class ConstantBindingBuilder {
        private Type service;
        private object value;

        internal static ConstantBindingBuilder Create(Type service, object value) {
            if (!service.IsAssignableFrom(value.GetType())) {
                throw new ServiceIsNotAssignableFromImplementation(service, value.GetType());
            }

            return new ConstantBindingBuilder(service, value);
        }

        private ConstantBindingBuilder(Type service, object value) {
            this.service = service;
            this.value = value;
        }

        public IBinding Build() {
            return new Binding(service, value.GetType(), new ConstantProvider(value));
        }
    }
}
