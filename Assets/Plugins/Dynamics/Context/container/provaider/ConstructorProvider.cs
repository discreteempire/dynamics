﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Dynamics.Context {
    public class ConstructorProvider : IProvider {
        private ConstructorInfo constructorInfo;
        private readonly Type type;

        public List<Type> parameters {
            get;
            private set;
        }

        internal static ConstructorProvider Create(Type type) {
            ConstructorInfo[] constructorInfos = type.GetConstructors();
            if (constructorInfos.Length > 1) {
                throw new OneConstructorAllowedException(type);
            }

            if (constructorInfos.Length == 1) {
                return new ConstructorProvider(type, constructorInfos[0]);
            } else {
                return new ConstructorProvider(type);
            }
        }

        private ConstructorProvider(Type type) {
            this.type = type;
            parameters = new List<Type>();
        }

        private ConstructorProvider(Type type, ConstructorInfo constructorInfo) : this(type) {
            this.constructorInfo = constructorInfo;
            parameters = GetParameters(constructorInfo);
        }
        
        private List<Type> GetParameters(ConstructorInfo constructor) {
            return constructor.GetParameters().Select(param => param.ParameterType).ToList();
        }

        public object Create(IContainer container) {
            if (constructorInfo == null) {
                return Activator.CreateInstance(type);
            } else {
                return constructorInfo.Invoke(ResolveInjections(container));
            }
        }

        private object[] ResolveInjections(IContainer container) {
            return container.Resolve(parameters).ToArray();
        }
    }
}
