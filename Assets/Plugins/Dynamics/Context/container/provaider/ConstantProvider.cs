﻿using System;
using System.Collections.Generic;

namespace Dynamics.Context {
    public class ConstantProvider : IProvider {
        private readonly object value;

        internal ConstantProvider(object value) {
            this.value = value;
        }

        public List<Type> parameters {
            get {
                return new List<Type>();
            }
        }

        public object Create(IContainer container) {
            return value;
        }
    }
}
