﻿using System;
using System.Collections.Generic;

namespace Dynamics.Context {
    public interface IProvider {
        List<Type> parameters { get; }

        object Create(IContainer container);
    }
}