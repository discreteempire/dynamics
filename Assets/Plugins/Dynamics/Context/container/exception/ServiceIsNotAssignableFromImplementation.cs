﻿using System;

namespace Dynamics.Context {
    public class ServiceIsNotAssignableFromImplementation : Exception {
        internal ServiceIsNotAssignableFromImplementation(Type service, Type implementation)
            : base(String.Format("Service {0} is not assignable from {1}", service, implementation)) {
        }
    }
}