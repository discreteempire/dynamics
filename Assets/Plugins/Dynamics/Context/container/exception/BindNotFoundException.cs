﻿using System;

namespace Dynamics.Context {
    public class BindNotFoundException : Exception {
        internal BindNotFoundException(Type type) 
            : base("Bind not found for: " + type.Name) {
        }
    }
}
