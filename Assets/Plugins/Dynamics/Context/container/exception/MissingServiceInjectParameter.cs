﻿using System;

namespace Dynamics.Context {
    public class MissingServiceInjectParameter : Exception {
        internal MissingServiceInjectParameter(Type service, Type parameter)
            : base(String.Format("Missing {0} parameter for {1} service constructor", parameter, service)) {
        }
    }
}