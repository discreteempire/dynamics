﻿using System;

namespace Dynamics.Context {
    public class OneConstructorAllowedException : Exception {
        internal OneConstructorAllowedException(Type type)
            : base(String.Format("Zero or one constructor allowed on service {0}", type)) {
        }
    }
}