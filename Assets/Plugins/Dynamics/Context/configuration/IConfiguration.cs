﻿using System.Collections.Generic;

namespace Dynamics.Context {
    public interface IConfiguration {
        IEnumerable<IBinding> bindings { get; }
    }
}
