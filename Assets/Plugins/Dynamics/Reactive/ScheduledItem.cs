﻿using System;

namespace Dynamics.Reactive {
    internal class ScheduledItem {
        private Action action;
        private BooleanDisposable disposable;

        public bool isCanceled {
            get {
                return disposable.isDisposed;
            }
        }

        public IDisposable cancelation {
            get {
                return disposable;
            }
        }

        public ScheduledItem(Action action) {
            this.action = action;

            disposable = new BooleanDisposable();
        }

        public void Invoke() {
            action();
        }
    }
}