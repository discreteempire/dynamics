﻿using System;

namespace Dynamics.Reactive {
    internal class BooleanDisposable : IDisposable {
        public bool isDisposed { get; private set; }

        public void Dispose() {
            isDisposed = true;
        }
    }
}