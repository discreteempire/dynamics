﻿using System;

namespace Dynamics.Reactive {
    public class CurrentThreadScheduler : IScheduler {
        [ThreadStatic]
        private static SchedulerQueue queue;

        public IDisposable Schedule(Action action) {
            ScheduledItem item = new ScheduledItem(action);

            if(queue == null) {
                queue = new SchedulerQueue();
                queue.Enqueue(item);

                try {
                    process();
                } finally {
                    queue = null;
                }
            } else {
                queue.Enqueue(item);
            }

            return item.cancelation;
        }

        private void process() {
            while (queue.Count > 0) {
                var item = queue.Dequeue();
                if (!item.isCanceled) {
                    if (!item.isCanceled) item.Invoke();
                }
            }
        }
    }
}
