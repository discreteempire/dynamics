﻿using System;

namespace Dynamics.Reactive {
    public interface IScheduler {
        IDisposable Schedule(Action action);
    }
}
