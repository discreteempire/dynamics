﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Dynamics.Context.Tests {
    [TestClass()]
    public class ContextTests {
        class TestClass {
        }

        class TestClass1 {
            public TestClass testClass;

            public TestClass1(TestClass testClass) {
                this.testClass = testClass;
            }
        }

        class TestConfiguration : IConfiguration {
            public IEnumerable<IBinding> bindings {
                get {
                    return new List<IBinding>() {
                        BindingBuilder.Bind<TestClass>().Build(),
                        BindingBuilder.Bind<TestClass1>().Build()
                    };
                }
            }
        }

        IDisposable registaration;

        [TestInitialize]
        public void Initialize() {
            registaration = Context.AddConfigution(new TestConfiguration());
        }

        [TestCleanup()]
        public void Cleanup() {
            if(registaration != null) {
                registaration.Dispose();
            }
        }

        [TestMethod()]
        public void ShouldAddConfiguration() {
            Assert.IsNotNull(Context.Resolve<TestClass>());
            Assert.IsNotNull(Context.Resolve<TestClass1>());
        }

        [TestMethod()]
        [ExpectedException(typeof(BindNotFoundException))]
        public void ShouldDisposeConfiguration() {
            registaration.Dispose();

            Context.Resolve<TestClass>();
        }

        [TestMethod()]
        public void ResolveTest() {
            TestClass testClass = Context.Resolve<TestClass>();
            TestClass1 testClass1 = Context.Resolve<TestClass1>();

            Assert.AreEqual(testClass, testClass1.testClass);
        }
    }
}