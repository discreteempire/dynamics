﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dynamics.Context.Tests {
    [TestClass()]
    public class BindingBuilderTests {
        interface TestInterface {
        }

        class TestClass : TestInterface {
            public TestClass() {
            }
        }

        class InvalidTestClass {
            public InvalidTestClass() {
            }

            public InvalidTestClass(int number) {
            }
        }

        [TestMethod()]
        public void ShouldCreateBindingWithConstructorProvicer() {
            IBinding binding = BindingBuilder.Bind<TestClass>().Build();

            Assert.AreEqual(typeof(TestClass), binding.service);
            Assert.AreEqual(typeof(TestClass), binding.implementation);
            Assert.IsInstanceOfType(binding.provider, typeof(ConstructorProvider));
        }

        [TestMethod()]
        public void ShouldCreateInterfaceBindingWithConstructorProvicer() {
            IBinding binding = BindingBuilder.Bind<TestInterface>().To<TestClass>().Build();

            Assert.AreEqual(typeof(TestInterface), binding.service);
            Assert.AreEqual(typeof(TestClass), binding.implementation);
            Assert.IsInstanceOfType(binding.provider, typeof(ConstructorProvider));
        }

        [TestMethod()]
        [ExpectedException(typeof(ServiceIsNotAssignableFromImplementation))]
        public void ShouldThrowExceptionWhenServiceTypeIsNotAssignableFromImplementationType() {
            BindingBuilder.Bind<TestInterface>().To<InvalidTestClass>().Build();
        }

        [TestMethod()]
        [ExpectedException(typeof(OneConstructorAllowedException))]
        public void ShouldThrowExceptionWhenTwoConstructorInImplementation() {
            BindingBuilder.Bind<InvalidTestClass>().Build();
        }

        [TestMethod()]
        public void ShouldCreateBindingWithConstantProvicer() {
            TestClass instance = new TestClass();

            IBinding binding = BindingBuilder.Bind<TestClass>().ToConstant(instance).Build();

            Assert.AreEqual(typeof(TestClass), binding.service);
            Assert.AreEqual(typeof(TestClass), binding.implementation);
            Assert.IsInstanceOfType(binding.provider, typeof(ConstantProvider));
        }

        [TestMethod()]
        public void ShouldCreateInterfaceBindingWithConstantProvicer() {
            TestClass instance = new TestClass();

            IBinding binding = BindingBuilder.Bind<TestInterface>().ToConstant(instance).Build();

            Assert.AreEqual(typeof(TestInterface), binding.service);
            Assert.AreEqual(typeof(TestClass), binding.implementation);
            Assert.IsInstanceOfType(binding.provider, typeof(ConstantProvider));
        }

        [TestMethod()]
        [ExpectedException(typeof(ServiceIsNotAssignableFromImplementation))]
        public void ShouldThrowExceptionWhenServiceTypeIsNotAssignableFromConstantValueType() {
            BindingBuilder.Bind<TestInterface>().ToConstant(new InvalidTestClass());
        }
    }
}