﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Dynamics.Context {
    [TestClass()]
    public class ContainerTests {

        class TestClass {
        }

        class TestClass1 {
            public TestClass testCalss;

            public TestClass1(TestClass testCalss) {
                this.testCalss = testCalss;
            }
        }
        
        class CycleTestClass2 {
            public CycleTestClass3 testCalss;

            public CycleTestClass2(CycleTestClass3 testCalss) {
                this.testCalss = testCalss;
            }
        }

        class CycleTestClass3 {
            public CycleTestClass4 testCalss;

            public CycleTestClass3(CycleTestClass4 testCalss) {
                this.testCalss = testCalss;
            }
        }

        class CycleTestClass4 {
            public CycleTestClass2 testCalss;

            public CycleTestClass4(CycleTestClass2 testCalss) {
                this.testCalss = testCalss;
            }
        }

        IContainer container;

        TestClass constant;
        List<IBinding> testClassConstantBinding;
        List<IBinding> testClassBinding;
        List<IBinding> testClass1Binding;
        List<IBinding> noCycleBinding;
        List<IBinding> cycleBinding;

        [TestInitialize]
        public void TestInitialize() {
            container = new Container();

            constant = new TestClass();
            testClassConstantBinding = new List<IBinding>() {
                BindingBuilder.Bind<TestClass>().ToConstant(constant).Build()
            };

            testClassBinding = new List<IBinding>() {
                BindingBuilder.Bind<TestClass>().Build()
            };

            testClass1Binding = new List<IBinding>() {
                BindingBuilder.Bind<TestClass1>().Build()
            };

            noCycleBinding = new List<IBinding>() {
                BindingBuilder.Bind<TestClass>().Build(),
                BindingBuilder.Bind<TestClass1>().Build(),
            };

            cycleBinding = new List<IBinding>() {
                BindingBuilder.Bind<CycleTestClass2>().Build(),
                BindingBuilder.Bind<CycleTestClass3>().Build(),
                BindingBuilder.Bind<CycleTestClass4>().Build()
            };
        }

        [TestMethod()]
        public void ShouldAddBindings() {
            container.AddBindings(testClassConstantBinding);

            Assert.AreEqual(constant, container.Resolve(typeof(TestClass)));
        }

        [TestMethod()]
        [ExpectedException(typeof(BindNotFoundException))]
        public void ShouldRemoveBindsTest() {
            container.AddBindings(testClassConstantBinding);

            container.RemoveBindings(testClassConstantBinding);

            Assert.AreEqual(constant, container.Resolve(typeof(TestClass)));
        }

        [TestMethod()]
        public void ShouldResolveTheSameObjectTwice() {
            container.AddBindings(testClassConstantBinding);

            Assert.AreEqual(constant, container.Resolve(typeof(TestClass)));
            Assert.AreEqual(constant, container.Resolve(typeof(TestClass)));
        }

        [TestMethod()]
        public void ShouldResolveTestClass1WithInjectedConstantTestClass() {
            container.AddBindings(testClassConstantBinding);
            container.AddBindings(testClass1Binding);

            TestClass1 resolved = (TestClass1)container.Resolve(typeof(TestClass1));

            Assert.IsNotNull(resolved.testCalss);
            Assert.AreEqual(constant, resolved.testCalss);
        }

        [TestMethod()]
        public void ShouldResolveTestClass1WithInjectedCreatedTestClass() {
            container.AddBindings(noCycleBinding);

            TestClass1 resolved = (TestClass1)container.Resolve(typeof(TestClass1));

            Assert.IsNotNull(resolved.testCalss);
        }

        [TestMethod()]
        [ExpectedException(typeof(MissingServiceInjectParameter))]
        public void ShouldThrowExceptionWhenServiceMissingInContainer() {
            container.AddBindings(testClass1Binding);
        }

        [TestMethod()]
        [ExpectedException(typeof(CycleFoundInBindings))]
        public void ShouldThrowExceptionWhenCycle() {
            container.AddBindings(cycleBinding);
        }
    }
}