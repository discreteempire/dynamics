﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Dynamics.Context.Tests {
    [TestClass()]
    public class LocalContextTests {
        class TestClass {
        }

        class TestClass1 {
            public TestClass testClass;

            public TestClass1(TestClass testClass) {
                this.testClass = testClass;
            }
        }

        class TestConfiguration : IConfiguration {
            public IEnumerable<IBinding> bindings {
                get {
                    return new List<IBinding>() {
                        BindingBuilder.Bind<TestClass>().Build(),
                        BindingBuilder.Bind<TestClass1>().Build()
                    };
                }
            }
        }

        IDisposable registaration;
        IContext context = new LocalContext();

        [TestInitialize]
        public void Initialize() {
            registaration = context.AddConfigution(new TestConfiguration());
        }
        
        [TestMethod()]
        public void ShouldAddConfiguration() {
            Assert.IsNotNull(context.Resolve<TestClass>());
            Assert.IsNotNull(context.Resolve<TestClass1>());
        }

        [TestMethod()]
        [ExpectedException(typeof(BindNotFoundException))]
        public void ShouldDisposeConfiguration() {
            registaration.Dispose();

            context.Resolve<TestClass>();
        }

        [TestMethod()]
        public void ResolveTest() {
            TestClass testClass = context.Resolve<TestClass>();
            TestClass1 testClass1 = context.Resolve<TestClass1>();

            Assert.AreEqual(testClass, testClass1.testClass);
        }
    }
}