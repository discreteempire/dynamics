﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dynamics.CommandHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using Dynamics.Reactive;

namespace Dynamics.CommandHandling.Tests {
    [TestClass()]
    public class CommandDispatcherTests {

        class TestCommand0 {
        }

        class TestCommand1 {
        }

        class TestCommand2 {
        }

        class TestException : Exception {
        }

        CommandDispatcher dispatcher;
        ICommandHandler[] handlers = new ICommandHandler[3] {
            Substitute.For<ICommandHandler>(),
            Substitute.For<ICommandHandler>(),
            Substitute.For<ICommandHandler>()
        };
        ICommandMessage[] messages = new ICommandMessage[3] {
            Substitute.For<ICommandMessage>(),
            Substitute.For<ICommandMessage>(),
            Substitute.For<ICommandMessage>()
        };
        object[] payloads = new object[] {
            new object(),
            new object(),
            new object()
        };

        [TestInitialize]
        public void Initialize() {
            IScheduler scheduler = new CurrentThreadScheduler();
            dispatcher = new CommandDispatcher(scheduler);

            messages[0].type.Returns(typeof(TestCommand0));
            messages[0].payload.Returns(payloads[0]);
            messages[1].type.Returns(typeof(TestCommand1));
            messages[1].payload.Returns(payloads[1]);
            messages[2].type.Returns(typeof(TestCommand2));
            messages[2].payload.Returns(payloads[2]);

            handlers[0].type.Returns(typeof(TestCommand0));
            handlers[1].type.Returns(typeof(TestCommand1));
            handlers[2].type.Returns(typeof(TestCommand2));
        }


        [TestMethod()]
        public void ShouldSbuscribeCommandHandler() {
            IDisposable subscription0 = dispatcher.Subscribe(handlers[0]);
            IDisposable subscription1 = dispatcher.Subscribe(handlers[1]);
            IDisposable subscription2 = dispatcher.Subscribe(handlers[2]);

            Assert.IsNotNull(subscription0);
            Assert.IsNotNull(subscription1);
            Assert.IsNotNull(subscription2);
        }

        [TestMethod()]
        public void ShouldDispatchCommandMessage() {
            dispatcher.Subscribe(handlers[0]);
            dispatcher.Subscribe(handlers[1]);
            dispatcher.Subscribe(handlers[2]);

            dispatcher.OnNext(messages[1]);

            handlers[0].DidNotReceive().OnNext(payloads[1]);
            handlers[1].Received().OnNext(payloads[1]);
            handlers[2].DidNotReceive().OnNext(payloads[1]);
        }

        [TestMethod()]
        public void ShouldHandleDispatchCommandMessageError() {
            Exception exception = new Exception();
            handlers[1]
                .When(h => h.OnNext(payloads[1]))
                .Do(h => throw exception);
            dispatcher.Subscribe(handlers[0]);
            dispatcher.Subscribe(handlers[1]);
            dispatcher.Subscribe(handlers[2]);

            dispatcher.OnNext(messages[1]);

            handlers[0].DidNotReceive().OnError(exception);
            handlers[1].Received().OnError(exception);
            handlers[2].DidNotReceive().OnError(exception);
        }

        [TestMethod()]
        public void ShouldPassOnCompleteToHandler() {
            dispatcher.Subscribe(handlers[0]);
            dispatcher.Subscribe(handlers[1]);
            dispatcher.Subscribe(handlers[2]);

            dispatcher.OnCompleted();

            handlers[0].Received().OnCompleted();
            handlers[1].Received().OnCompleted();
            handlers[2].Received().OnCompleted();
        }

        [TestMethod()]
        [ExpectedException(typeof(CommandHandlerNotFoundException))]
        public void ShouldUnsubscribeCommandHandler() {
            dispatcher.Subscribe(handlers[0]);
            IDisposable subscription = dispatcher.Subscribe(handlers[1]);
            dispatcher.Subscribe(handlers[2]);

            subscription.Dispose();
            dispatcher.OnNext(messages[1]);
        }
    }
}