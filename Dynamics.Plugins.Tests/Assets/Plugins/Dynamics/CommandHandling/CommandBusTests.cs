﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dynamics.Reactive;
using System;
using NSubstitute;

namespace Dynamics.CommandHandling.Tests {
    [TestClass()]
    public class CommandBusTests {

        class TestException : Exception {
        }

        CommandBus commandBus;
        ICommandObserver[] observers = new ICommandObserver[3] {
            Substitute.For<ICommandObserver>(),
            Substitute.For<ICommandObserver>(),
            Substitute.For<ICommandObserver>()
        };
        ICommandMessage message = Substitute.For<ICommandMessage>();

        [TestInitialize]
        public void Initialize() {
            IScheduler scheduler = new CurrentThreadScheduler();
            commandBus = new CommandBus(scheduler);
        }

        [TestMethod()]
        public void ShouldSbuscribeCommandObserver() {
            IDisposable subscription0 = commandBus.Subscribe(observers[0]);
            IDisposable subscription1 = commandBus.Subscribe(observers[1]);
            IDisposable subscription2 = commandBus.Subscribe(observers[2]);

            Assert.IsNotNull(subscription0);
            Assert.IsNotNull(subscription1);
            Assert.IsNotNull(subscription2);
        }

        [TestMethod()]
        public void ShouldDispatchCommandMessage() {
            commandBus.Subscribe(observers[0]);
            commandBus.Subscribe(observers[1]);
            commandBus.Subscribe(observers[2]);

            commandBus.Dispatch(message);

            observers[0].Received().OnNext(message);
            observers[1].Received().OnNext(message);
            observers[2].Received().OnNext(message);
        }

        [TestMethod()]
        public void ShouldHandleCommandObserverError() {
            TestException exception = new TestException();
            observers[1].When(o => o.OnNext(message))
                    .Do(o => throw exception);
            commandBus.Subscribe(observers[0]);
            commandBus.Subscribe(observers[1]);
            commandBus.Subscribe(observers[2]);

            commandBus.Dispatch(message);

            observers[0].Received().OnNext(message);
            observers[1].Received().OnError(exception);
            observers[2].Received().OnNext(message);
        }

        [TestMethod()]
        public void ShouldUnsubscribeCommandObserver() {
            commandBus.Subscribe(observers[0]);
            IDisposable subscription = commandBus.Subscribe(observers[1]);
            commandBus.Subscribe(observers[2]);

            subscription.Dispose();
            commandBus.Dispatch(message);

            observers[0].Received().OnNext(message);
            observers[1].DidNotReceive().OnNext(message);
            observers[1].Received().OnCompleted();
            observers[2].Received().OnNext(message);
        }

        [TestMethod()]
        public void ShouldDisposeCommandBus() {
            commandBus.Subscribe(observers[0]);
            commandBus.Subscribe(observers[1]);
            commandBus.Subscribe(observers[2]);

            commandBus.Dispose();

            observers[0].Received().OnCompleted();
            observers[1].Received().OnCompleted();
            observers[2].Received().OnCompleted();
        }
    }
}