﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dynamics.CommandHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dynamics.CommandHandling.Tests {
    [TestClass()]
    public class CommandHandlerTests {

        class TestCommand {
            public int value;

            public TestCommand(int value) {
                this.value = value;
            }
        }

        [TestMethod()]
        public void ShouldCallActionOnNext() {
            int value = 0;
            ICommandHandler handler = CommandHandler.Create<TestCommand>(c => value = c.value);

            handler.OnNext(new TestCommand(10));

            Assert.AreEqual(10, value);
        }
    }
}