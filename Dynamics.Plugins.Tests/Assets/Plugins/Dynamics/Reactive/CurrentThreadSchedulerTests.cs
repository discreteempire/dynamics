﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Dynamics.Reactive.Tests {
    [TestClass()]
    public class CurrentThreadSchedulerTests {

        CurrentThreadScheduler scheduler = new CurrentThreadScheduler();

        [TestMethod()]
        public void ShouldScheduleAction() {
            bool invoked = false;

            scheduler.Schedule(() => invoked = true);

            Assert.IsTrue(invoked);
        }

        [TestMethod()]
        public void ShouldScheduleTwoActions() {
            bool firstInvoked = false;
            bool secondInvoked = false;

            scheduler.Schedule(() => {
                firstInvoked = true;
                scheduler.Schedule(() => secondInvoked = true);
            });

            Assert.IsTrue(firstInvoked);
            Assert.IsTrue(secondInvoked);
        }

        [TestMethod()]
        public void ShouldProcessActionInAddingOrder() {
            int i = 0;
            int firstInvoke = 0;
            int secondInvoke = 0;

            scheduler.Schedule(() => firstInvoke = ++i);
            scheduler.Schedule(() => secondInvoke = ++i);

            Assert.AreEqual(1, firstInvoke);
            Assert.AreEqual(2, secondInvoke);
        }

        [TestMethod()]
        public void ShouldProcessActionInAddingOrder1() {
            int i = 0;
            int firstInvoke = 0;
            int secondInvoke = 0;

            scheduler.Schedule(() => {
                firstInvoke = ++i;

                scheduler.Schedule(() => secondInvoke = ++i);
            });

            Assert.AreEqual(1, firstInvoke);
            Assert.AreEqual(2, secondInvoke);
        }

        [TestMethod()]
        public void ShouldCancelAction() {
            bool firstInvoked = false;
            bool secondInvoked = false;
            bool thirdInvoked = false;

            scheduler.Schedule(() => {
                firstInvoked = true;

                IDisposable cancellation = scheduler.Schedule(() => secondInvoked = true);
                scheduler.Schedule(() => thirdInvoked = true);

                cancellation.Dispose();
            });

            Assert.IsTrue(firstInvoked);
            Assert.IsFalse(secondInvoked);
            Assert.IsTrue(thirdInvoked);
        }
    }
}